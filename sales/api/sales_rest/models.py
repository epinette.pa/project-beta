from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
  vin=models.CharField(max_length=200, unique=True)
  sold=models.BooleanField(default=False)

  def __str__(self):
        return self.vin

class Salesperson(models.Model):
 first_name=models.CharField(max_length=15)
 last_name=models.CharField(max_length=20)
 employee_id=models.CharField(max_length=7, unique=True)

 def __str__(self):
        return self.first_name


class Customer(models.Model):
 first_name= models.CharField(max_length=15)
 last_name=models.CharField(max_length=20)
 address=models.CharField(max_length=100)
 phone_number=models.CharField(max_length=15)

 def __str__(self):
        return self.first_name


class Sale(models.Model):
 automobile=models.ForeignKey(
   AutomobileVO,
   related_name="autoVO",
   on_delete=models.CASCADE

 )
 salesperson=models.ForeignKey(
    Salesperson,
    related_name="salesperson",
    on_delete=models.CASCADE,
 )
 customer=models.ForeignKey(
    Customer,
    related_name="customer",
    on_delete=models.CASCADE,
 )
 price=models.BigIntegerField()
