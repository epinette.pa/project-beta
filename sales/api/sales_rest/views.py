from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import  AutomobileVO, Customer,Salesperson, Sale
from .encoders import CustomerEncoder, SalesPersonEncoder,SaleEncoder





@require_http_methods(["GET","POST"])
def customers_api(request):
   if request.method == "GET":
        customers = Customer.objects.all()
        response= JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
        response.status_code=200
        return response
   else:#POST
      try:
          content=json.loads(request.body)
          customer=Customer.objects.create(**content)
          response=JsonResponse(
               customer,
               encoder=CustomerEncoder,
               safe=False,
         )
          response.status_code=201
          return response
      except:
           response=JsonResponse(
               {"message":"Did not create customer"}
           )
           response.status_code = 400
           return response


@require_http_methods(["DELETE"])
def delete_customer_api(request,id):
    try:
        customer=Customer.objects.get(id=id)
        customer.delete()
        response=JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
        response.status_code=200
        return response
    except Customer.DoesNotExist:
        response=JsonResponse(
            {"message":"Customer doesn't exist"}
        )
        response.status_code = 400
        return response



@require_http_methods(["GET","POST"])
def salesperson_api(request):
   if request.method == "GET":
        salespersons = Salesperson.objects.all()
        response= JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder,
            safe=False,
        )
        response.status_code=200
        return response
   else:
    try:
        content=json.loads(request.body)
        salesperson=Salesperson.objects.create(**content)
        response=JsonResponse(
               salesperson,
               encoder=SalesPersonEncoder,
               safe=False,
        )
        response.status_code=201
        return response
    except:
           response=JsonResponse(
               {"message":"Did not create SalesPerson"}
           )
           response.status_code = 400
           return response

@require_http_methods(["GET", "POST"])
def sale_api(request):
 if request.method == "GET":
     sales= Sale.objects.all()
     response=JsonResponse(
         {"sales":sales},
         encoder=SaleEncoder,
         safe=False,
     )
     response.status_code=200
     return response
 else:#POST
   try:
     content= json.loads(request.body)

     automobile_vin=content["automobile"]
     automobile=AutomobileVO.objects.get(vin=automobile_vin , sold=False)
     content["automobile"]= automobile

     salesperson_id=content["salesperson"]
     salesperson= Salesperson.objects.get(id=salesperson_id)
     content["salesperson"]=salesperson

     customer_id=content["customer"]
     customer=Customer.objects.get(id=customer_id)
     content["customer"]=customer

     automobile.sold= True
     automobile.save()

     sale=Sale.objects.create(**content)
     response= JsonResponse(
            sale,
            encoder=SaleEncoder,
         safe=False
         )
     response.status_code=201
     return response
   except:
     response=JsonResponse(
    {"message":"Did not record a Sale"}
           )
     response.status_code = 400
     return response
