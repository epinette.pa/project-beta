---
creation_date: 2023-06-09
modification_date: 2023-06-09
---

# CarCar

The premiere solution for automobile dealership management ! CarCar allows you to easily manage the inventory, automobile sales and services.

The app uses a Django RESTful API in the back-end, and React in the front-end to display interact with the data.

Team:

- Paul-Adrien Epinette - Service microservice
- Alec Weinstein - Sales microservice

## Table of Contents

- [Getting Started](#getting-started)

- [Design](#design)

- [Inventory Microservice](#inventory-microservice)

- [Service Microservice](#service-microservice)

- [Sales Microservice](#sales-microservice)

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above, having Insomnia is recommended.**

1. Fork this repository
2. Clone the forked repository onto your local computer inside the directory of your choice.
3. Build and run the project from the cloned repository using Docker with these commands:

```
docker volume create beta-data
docker-compose build
docker-compose up
```

- After running these commands, make sure all of your Docker containers are running
- View the project in the browser: [http://localhost:3000/](http://localhost:3000/)

## Design

The app CarCar is currently composed of 3 microservices that interact with one another.
Both Sales and Service microservices poll Automobile data from the inventory, and use the Automobile Value Object to perform tasks such as recording sales or defining if an appointment has been made for a car sold by the dealership.

### Context Map

<img src="ghi/app/src/images/CarCar_Context-map.png">

## Inventory Microservice

The inventory allows users to fetch, create, update and delete data entries for the following:

- Manufacturer
- Vehicle Model
- Automobile

### Accessing Endpoints to Send and View Data: Access Through Insomnia & Browser

#### Manufacturers

##### RESTful API Manufacturers' Endpoints (Port 8100)

| Action                         | Method | URL                                            |
| :----------------------------- | :----- | :--------------------------------------------- |
| List manufacturers             | GET    | `http://localhost:8100/api/manufacturers/`     |
| Create a manufacturer          | POST   | `http://localhost:8100/api/manufacturers/`     |
| Get a specific manufacturer    | GET    | `http://localhost:8100/api/manufacturers/:id/` |
| Update a specific manufacturer | PUT    | `http://localhost:8100/api/manufacturers/:id/` |
| Delete a specific manufacturer | DELETE | `http://localhost:8100/api/manufacturers/:id/` |

##### JSON Body to Send / Update Data

<details>
<summary>Creating and updating a manufacturer</summary>

```
{
  "name": "Peugeot"
}
```

</details>
<details>
<summary>JSON response for fetching, creating, and updating a  manufacturer</summary>

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Citroën"
}
```

</details>
<details>
<summary>JSON response for list of manufacturers</summary>

```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Citroën"
		},
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Peugeot"
		}
	]
}
```

</details>

#### Vehicle Models

##### RESTful API Vehicle Models' Endpoints (Port 8100)

| Action                                   | Method | URL                                     |
| :--------------------------------------- | :----- | :-------------------------------------- |
| List vehicle models                      | GET    | `http://localhost:8100/api/models/`     |
| Create a vehicle model                   | POST   | `http://localhost:8100/api/models/`     |
| Get a specific vehicle model             | GET    | `http://localhost:8100/api/models/:id/` |
| Update a specific specific vehicle model | PUT    | `http://localhost:8100/api/models/:id/` |
| Delete a specific vehicle model          | DELETE | `http://localhost:8100/api/models/:id/` |

<details>
<summary>Creating a vehicle model</summary>

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

</details>
<details>
<summary>Updating vehicle model input</summary>

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Peugeot"
	}
}
```

</details>
<details>
<summary>JSON response for fetching, creating, and updating  a vehicle model</summary>

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Peugeot"
	}
}
```

</details>
<details>
<summary>JSON response for list of vehicle models</summary>

```
{
	"models": [
		{
			"href": "/api/models/2/",
			"id": 2,
			"name": "C4",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/citroen_c4.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/2/",
				"id": 2,
				"name": "Citroën"
			}
		},
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Peugeot"
			}
		}
	]
}
```

</details>

#### Automobile

##### RESTful API Automobiles' Endpoints (Port 8100)

| Action                       | Method | URL                                           |
| :--------------------------- | :----- | :-------------------------------------------- |
| List automobiles             | GET    | `http://localhost:8100/api/automobiles/`      |
| Create an automobile         | POST   | `http://localhost:8100/api/automobiles/`      |
| Get a specific automobile    | GET    | `http://localhost:8100/api/automobiles/:vin/` |
| Update a specific automobile | PUT    | `http://localhost:8100/api/automobiles/:vin/` |
| Delete a specific automobile | DELETE | `http://localhost:8100/api/automobiles/:vin/` |

<details>
<summary>Creating an automobile</summary>

```
{
  "vin": "1C3CC5FB2ANP20177",
	"model_id": 2,
	"year": 2012,
	"color": "red"
}
```

</details>
<details>
<summary>JSON response for fetching specific automobile details</summary>

```
{
	"href": "/api/automobiles/1C3CC5FB2ANP20177/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2ANP20177",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "C4",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/citroen_c4.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Citroën"
		}
	},
	"sold": false
}
```

</details>
<details>
<summary>Updating the automobile</summary>

```
{
  "color": "red",
  "year": 2012,
  "model_id": 2,
	"sold": false
}
```

</details>
<details>
<summary>JSON response for a list of Automobiles</summary>

```
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2ANP20177/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2ANP20177",
			"model": {
				"href": "/api/models/2/",
				"id": 2,
				"name": "C4",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/citroen_c4.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Citroën"
				}
			},
			"sold": false
		},
		{
			"href": "/api/automobiles/F745FKI781POK63QW/",
			"id": 2,
			"color": "blue",
			"year": 2002,
			"vin": "F745FKI781POK63QW",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Peugeot"
				}
			},
			"sold": false
		}
	]
}
```

</details>

## Service Microservice

This microservice allows the following functionalities:

- Technicians
  - Create a new technician through a form
  - List all technicians from the dealership
- Service Appointments
  - Create a new service appointment
    - Enables VIP status for appointments made for cars sold by the dealership
  - List current and past appointments (finished or canceled)

### Accessing Endpoints to Send and View Data: Access Through Insomnia & Browser

#### Technician

##### RESTful API Technicians' Endpoints (Port 8080)

| Action                                | Method | URL                                          |
| :------------------------------------ | :----- | :------------------------------------------- |
| List technicians                      | GET    | `http://localhost:8080/api/technicians/`     |
| Create a technician                   | POST   | `http://localhost:8080/api/technicians/`     |
| Get a specific technician             | GET    | `http://localhost:8080/api/technicians/:id/` |
| Update a specific specific technician | PUT    | `http://localhost:8080/api/technicians/:id/` |
| Delete a specific technician          | DELETE | `http://localhost:8080/api/technicians/:id/` |

<details>
<summary>Creating and updating a technician</summary>

```
{
	"first_name" : "Jean",
	"last_name" : "Neymar"
}
```

</details>
<details>
<summary>JSON response for fetching, creating, and updating  a technician</summary>

```
{
	"first_name": "Jean",
	"last_name": "Neymar",
	"employee_id": 2
}
```

</details>
<details>
<summary>JSON response for a list of technicians</summary>

```
{
	"technicians": [
		{
			"first_name": "Manamiz",
			"last_name": "Jeff",
			"employee_id": 1
		},
		{
			"first_name": "Jean",
			"last_name": "Neymar",
			"employee_id": 2
		}
	]
}
```

</details>

#### Appointment

##### RESTful API Appointments' Endpoints (Port 8080)

| Action                                 | Method | URL                                                  |
| :------------------------------------- | :----- | :--------------------------------------------------- |
| List appointments                      | GET    | `http://localhost:8080/api/appointments/`            |
| Create an appointment                  | POST   | `http://localhost:8080/api/appointments/`            |
| Get a specific appointment             | GET    | `http://localhost:8080/api/appointments/:id/`        |
| Update a specific specific appointment | PUT    | `http://localhost:8080/api/appointments/:id/`        |
| Delete a specific appointment          | DELETE | `http://localhost:8080/api/appointments/:id/`        |
| Set appointment status to "CANCELED"   | PUT    | `http://localhost:8080/api/appointments/:id/cancel/` |
| Set appointment status to "FINISHED"   | PUT    | `http://localhost:8080/api/appointments/:id/finish/` |

<details>
<summary>Creating an appointment</summary>

```
{
			"vin": "QHYT762QA38IK96L3",
			"customer": "Brian O'Conner",
			"is_vip": "False",
			"date_time": "2023-11-29 17:30",
			"technician": "1",
			"reason": "Steering wheel issue",
			"status": "BOOKED"
}
```

</details>
<details>
<summary>Updating an appointment</summary>

```
{
			"vin": "QHYT762QA38IK96L3",
			"customer": "Brian O'Conner",
			"is_vip": "False",
			"date_time": "2023-11-29 17:30",
			"technician": "1",
			"reason": "Steering wheel issue",
			"status": "BOOKED"
}
```

</details>
<details>
<summary>JSON response for fetching, creating, and updating  an appointment</summary>

```
{
	"id": 1,
	"vin": "QHYT762QA38IK96L3",
	"customer": "Vin Gasoline",
	"is_vip": false,
	"date_time": "2023-11-11 09:30",
	"technician": {
		"first_name": "Jean",
		"last_name": "Neymar",
		"employee_id": 2
	},
	"reason": "Broken windshield",
	"status": "BOOKED"
}
```

</details>
<details>
<summary>JSON response for a list of appointments</summary>

```
{
	"appointments": [
		{
			"id": 2,
			"vin": "184KYF96P7SH145KJ",
			"customer": "Elric",
			"is_vip": false,
			"date_time": "2023-06-15T15:57:00+00:00",
			"technician": {
				"first_name": "Jean",
				"last_name": "Neymar",
				"employee_id": 2
			},
			"reason": "Not your business bruh",
			"status": "BOOKED"
		},
		{
			"id": 1,
			"vin": "QHYT762QA38IK96L3",
			"customer": "Vin Gasoline",
			"is_vip": false,
			"date_time": "2023-11-11T09:30:00+00:00",
			"technician": {
				"first_name": "Jean",
				"last_name": "Neymar",
				"employee_id": 2
			},
			"reason": "Broken windshield",
			"status": "BOOKED"
		}
	]
}
```

</details>

#### Value Objects

The Service microservice has one value object, AutomobileVO, which is the result of the data fetched from the poller to the Inventory's database for automobiles.
In this microservice it is mainly used to check whether an appointment has been made for a car previously purchased from the dealership, hence providing the customer with a special VIP status.

## Sales Microservice

On the backend, the sales microservice has 4 models: AutomobileVO, Customer, Salesperson, and Sale. The Sale model  interacts with the other three models. This model gets data from the three other models.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data about automobiles, so the sales microservice is constantly(every 10 seconds) getting the updated data. It then creates Automobile VO objects in the Sales Microservice.

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold(its vin) and that information lives inside of the inventory microservice.

This microservice allows the following functionalities:

- Customers
  - Create a new Customer through a form
  - List all of the dealership's customers
- Salespersons
  - Create a new Salesperson through a form
  - List all Salespersons
 - Sales
  - Create a new Sale
  - See a list of all Sales
    - See a complete list of all Sales conducted by each Salespersom


### Accessing Endpoints to Send and View Data: Access Through Insomnia & Browser

#### Customer

##### RESTful API Customer Endpoints (Port 8090)

| Action                                | Method | URL                                          |
| :------------------------------------ | :----- | :------------------------------------------- |
| List Customers                        | GET    | `http://localhost:8090/api/customers/`    |
| Create a Customer                     | POST   | `http://localhost:8090/api/customers/`   |


<details>
<summary>Creating a Customer </summary>

```

	{
  "first_name": "Harry",
	"last_name":"Kasapidis",
	"address": " 1050 Bushwick Ave Brooklyn NY",
	"phone_number":"917-332-4571"


    }

```

</details>
<details>
<summary>JSON response for fetching and creating a customer </summary>

```
{

	"first_name": "Harry",
	"last_name": "Kasapidis",
	"address": " 1050 Bushwick Ave Brooklyn NY",
	"phone_number": "917-332-4571",
	"id": 12

}
```

</details>
<details>
<summary>JSON response for a list of customers</summary>

```
{
	"customers": [
		{
			"first_name": "Sam",
			"last_name": "Smith",
			"address": "345 Grant Ave Woodmere New York 11598",
			"phone_number": "516-699-5555",
			"id": 2
		},
		{
			"first_name": "Harry",
			"last_name": "Kasapidis",
			"address": " 1050 Bushwick Ave Brooklyn NY",
			"phone_number": "917-332-4571",
			"id": 12
		},
	]
}
```

</details>

#### Salespersons

##### RESTful API Salespersons' Endpoints (Port 8090)

| Action                                 | Method | URL                                                  |
| :------------------------------------- | :----- | :--------------------------------------------------- |
| List Salespersons                      | GET    | `http://localhost:8090/api/salespeople/ `          |
| Create a new Salesperson               | POST   | `http://localhost:8090/api/salespeople/ `            |


<details>
<summary>Creating a salesperson</summary>

```
{
	"first_name":"Don",
	"last_name":"Draper",
	"employee_id":15

}
```
</details>



<details>
<summary>JSON response for a list of salespeople</summary>

```
{
	"salespersons": [
		{
			"first_name": "John",
			"last_name": "Donne",
			"employee_id": "1",
			"id": 1
		},
		{
			"first_name": "Sylvia",
			"last_name": "Plath",
			"employee_id": "2",
			"id": 2
		},
		{
			"first_name": "John",
			"last_name": "Keats",
			"employee_id": "3",
			"id": 3
		},
		{
			"first_name": "Don",
			"last_name": "Delillo",
			"employee_id": "WHT1985",
			"id": 4
		}
	]
}
```
</details>


#### Sales

##### RESTful API Sales Endpoints (Port 8090)

| Action                                 | Method | URL                                                  |
| :------------------------------------- | :----- | :--------------------------------------------------- |
| Create Sale                            | POST  | `http://localhost:8090/api/sales/`            |
| List Sales                             | GET  | `http://localhost:8090/api/sales/`            |

 To access a list of Sales filtered by the Salesperson who conducted them, we must utilize a frontend route resource.
 Please enter `http://localhost:3000/sales/history` in your browser. Upon rendering, you can selected from a dropdown list of salespersons names. Clicking one will populate the page with their respective sale histories.

<details>
<summary>Creating a Sale</summary>

```
{
	"automobile":"1C3CC5FB2AN178342",
	"salesperson": 3,
	"customer": 15 ,
	"price": 22000


}

```
Wherein the automobile value is equal to its vin, the salesperson value is their salesperson id(not their employee_id), and the customer value is their customer id. These values can be found in the Automobile List JSON response, the salesperson list JSON response, and the customer list JSON response respectively.

</details>

<details>
<summary>JSON response for a list of sales</summary>


```
{
	"sales": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN199994",
				"sold": false
			},
			"salesperson": {
				"first_name": "Sylvia",
				"last_name": "Plath",
				"employee_id": "2",
				"id": 2
			},
			"customer": {
				"first_name": "Harry",
				"last_name": "Kasapidis",
				"address": " 1050 Bushwick Ave Brooklyn NY",
				"phone_number": "917-332-4571",
				"id": 12
			},
			"price": 21000,
			"id": 7
		},
	]
}
```
</details>

#### Delete functionality

In order to be able to delete objects in the Sales Microservice, please follow these instructions:
1. We will be using the Django Admin feature to delete objects.
2. In order to do so, please find the container named "project-beta-sales-api-1" in your Docker Containers.
3. Run the following command `python manage.py createsuperuser` in the container's terminal.
4. Follow the instructions to create a User, then log into the admin portal at `http://localhost:8090/admin/`.
5. Inside the portal you can create or delete Customers, Sales, and Salespersons. Deleting Salespersons or Customers will delete their related Sales Records.
