import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import (
    AutomobileVO,
    Technician,
    Appointment
)
from .encoders import (
    AutomobileVOEncoder,
    TechnicianEncoder,
    AppointmentEncoder
)


@require_http_methods(["GET"])
def api_list_autovo(request):
    if request.method == "GET":
        autovos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autovos": autovos},
            encoder=AutomobileVOEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
        )
        except:
            response = JsonResponse(
                {"message": "Technician could not be created"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(pk=pk)
            return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.filter(pk=pk)
            technician.first_name = content["first_name"]
            technician.last_name = content["last_name"]
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )
    else:
        try:
            count, _ = Technician.objects.get(pk=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
            content = json.loads(request.body)
            try:
                technician = Technician.objects.get(employee_id=content["technician"])
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse(
                    {"message": "Appointment could not be created because the technician does not exist."},
                    status=400,
                )
            automobiles_vo = AutomobileVO.objects.all()
            if any(auto_vo.vin == content["vin"] for auto_vo in automobiles_vo):
                content["is_vip"] = True
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )


@require_http_methods(["GET","PUT", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(pk=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(pk=pk)
            appointment.date_time = content["date_time"]
            appointment.reason = content["reason"]
            appointment.customer = content["customer"]
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
            appointment.technician = content["technician"]
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )
    else:
        try:
            count, _ = Appointment.objects.get(pk=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.finished()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.canceled()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )
