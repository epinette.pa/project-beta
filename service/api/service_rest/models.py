from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    employee_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return self.last_name

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.employee_id})

    class Meta:
        ordering = ("last_name",)


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    is_vip = models.BooleanField(default=False)
    date_time = models.DateTimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    reason = models.TextField()
    status = models.CharField(max_length=30, default="BOOKED")

    def finished(self):
        self.status = "FINISHED"
        self.save()

    def canceled(self):
        self.status = "CANCELED"
        self.save()

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    class Meta:
        ordering = ("date_time",)
