import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './inventory/ManufacturersList';
import ManufacturerForm from './inventory/ManufacturerForm';
import ModelForm from './inventory/ModelForm';
import ModelsList from './inventory/ModelsList';
import AutomobileForm from './inventory/AutomobileForm';
import AutomobilesList from './inventory/AutomobilesList';
import TechniciansList from './service/TechniciansList';
import TechniciansForm from './service/TechnicianForm';
import CustomerForm from './sale/CustomerForm';
import CustomersList from './sale/CustomersList';
import AppointmentsList from './service/AppointmentsList'
import AppointmentForm from './service/AppointmentForm'
import SalesPersonForm from './sale/SalesPersonForm';
import SalesPersonsList from './sale/SalesPeopleList';
import SalesList from './sale/SalesList';
import SalesHistory from './sale/SalesHistory';
import CreateSaleForm from './sale/CreateSale';

function App() {
  const [ manufacturers, setManufacturers ] = useState([]);
  const [ automobiles, setAutomobiles ] = useState([]);
  const [ models, setModels ] = useState([]);
  const [ technicians, setTechnicians ] = useState([]);
  const [ customers , setCustomers ] = useState([]);
  const [ appointments, setAppointments ] = useState([]);
  const[salespersons, setSalesPersons]= useState([]);
  const[sales,setSales]=useState([]);

  async function getManufacturers() {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } else {
      console.error(response)
    }
  }

  async function getModels() {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error(response)
    }
  }

  async function getAutomobiles() {
    const url= 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if(response.ok){
      const data = await response.json();
      setAutomobiles(data.autos);
    } else {
      console.error(response)
    }
  }

  async function getTechnicians() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    } else {
      console.error(response);
    }
  }

  async function getCustomers() {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers)
    } else {
      console.error(response);
    }
  }

  async function getAppointments() {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    } else {
      console.error(response);
    }
  }
  async function getSalesPersons() {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.salespersons)
    } else {
      console.error(response);
    }
  }
  async function getSales() {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    getManufacturers();
    getModels();
    getAutomobiles();
    getTechnicians();
    getCustomers();
    getAppointments();
    getSalesPersons();
    getSales();
  }, []);

  async function deleteManufacturer(manufacturerId) {
    const url = `http://localhost:8100/api/manufacturers/${manufacturerId}/`
    const response = await fetch(url, {
      method: 'DELETE',
    });
    if (response.ok) {
      setManufacturers(manufacturers.filter((manufacturer) => manufacturer.id !== manufacturerId));
    } else {
      console.error(response)
    }
}

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList manufacturers={manufacturers} onDeleteManufacturer={deleteManufacturer}/>} />
            <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers} />} />
          </Route>
          <Route path= "models">
            <Route index element={<ModelsList models={models} />}/>
            <Route path="new" element={<ModelForm getModels={getModels} manufacturers={manufacturers} />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList automobiles={automobiles} />} />
            <Route path="new" element={<AutomobileForm  getAutomobiles={getAutomobiles} models={models}/>} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList technicians={technicians} />} />
            <Route path="new" element={<TechniciansForm getTechnicians={getTechnicians} />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList customers={customers} />}/>
            <Route path="new" element={<CustomerForm  getCustomers={getCustomers}/>}/>
          </Route>
          <Route path="salespeople">
            <Route index element={<SalesPersonsList salespersons={salespersons}/>}/>
            <Route path="new" element={<SalesPersonForm getSalesPersons={getSalesPersons}/>}/>
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList appointments={appointments} getAppointments={getAppointments} />} />
            <Route path="new" element={<AppointmentForm  getAppointments={getAppointments} appointments={appointments} technicians={technicians}/>} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList sales={sales}/>} />
            <Route path="new" element={<CreateSaleForm  getSales={getSales}/>}/>
            <Route path="history"element={<SalesHistory />}  />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
