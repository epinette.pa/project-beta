import { NavLink } from "react-router-dom";


function ManufacturersList({ manufacturers, onDeleteManufacturer }) {
  const handleDelete = (id) => {
    onDeleteManufacturer(id);
  }

  if (manufacturers === undefined) {
    return null;
  }


  return (
    <div>
      <h1 className="font-weight-bold mt-4"> Manufacturers </h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{ manufacturer.name }</td>
                <td>
                  <button onClick={() => handleDelete(manufacturer.id)}>DELETE</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="mb-3">
        <NavLink to="/manufacturers/new" className="btn btn-primary mb-3">Add a new Manufacturer</NavLink>
      </div>
    </div>
    );
  }

  export default ManufacturersList;
