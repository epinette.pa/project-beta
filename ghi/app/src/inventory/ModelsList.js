import { NavLink } from 'react-router-dom';


function ModelsList({ models }) {
    const styles = {width: '80px', height:'80px'};

    if (models === undefined) {
        return null;
    }


    return (
        <div>
        <h1 className="font-weight-bold mt-4"> Models </h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model=> {
                    return (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td><img style={styles} className="img-fluid rounded-circle shadow " src={model.picture_url} alt="ModelPic"></img></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <div className="mb-3">
          <NavLink to="/models/new" className="btn btn-primary mb-3">Add a new Model</NavLink>
        </div>
        </div>
    );

}

export default ModelsList;
