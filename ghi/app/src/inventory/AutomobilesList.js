import { NavLink } from "react-router-dom";


function AutomobilesList({ automobiles }) {

  if (automobiles === undefined) {
    return null;
  }


  return (
        <div>
          <h1 className="font-weight-bold mt-4"> Automobiles </h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Manufacturer</th>
                <th>Model</th>
                <th>Color</th>
                <th>Year</th>
                <th>Sold</th>
              </tr>
            </thead>
            <tbody>
              {automobiles.map(auto => {
                const soldStatus = auto.sold ? "Yes" : "No";
                return (
                  <tr key={auto.vin}>
                    <td>{auto.vin}</td>
                    <td>{auto.model.manufacturer.name}</td>
                    <td>{auto.model.name}</td>
                    <td>{auto.color}</td>
                    <td>{auto.year}</td>
                    <td>{soldStatus}</td>
                 </tr>
                );
              })}
            </tbody>
          </table>
          <div className="mb-3">
            <NavLink to="/automobiles/new" className="btn btn-primary mb-3">Add an Automobile</NavLink>
          </div>
        </div>
      );
}

export default  AutomobilesList;
