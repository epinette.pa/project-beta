

function SalesPersonsList({salespersons}){



return(
    <div>
    <h1 className="font-weight-bold mt-4"> Salespersons </h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee Id</th>

        </tr>
      </thead>
      <tbody>
        {salespersons.map(salesperson => {
          return (
            <tr key={salesperson.id}>
              <td>{ salesperson.first_name}</td>
              <td>{ salesperson.last_name }</td>
              <td>{ salesperson.employee_id }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </div>




)


}


export default SalesPersonsList
