import { NavLink } from "react-router-dom";


function TechniciansList({ technicians }) {

  if (technicians === undefined) {
    return null;
  }


  return (
    <>
    <div>
    <h1 className="font-weight-bold mt-4"> Technicians </h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Last Name</th>
          <th>First Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map(technician => {
          return (
            <tr key={technician.employee_id}>
              <td>{ technician.last_name }</td>
              <td>{ technician.first_name }</td>
              <td>{ technician.employee_id }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    <div className="mb-3">
      <NavLink to="/technicians/new" className="btn btn-primary mb-3">Add a new Technician</NavLink>
    </div>
    </div>
    </>
  )
}

export default TechniciansList;
